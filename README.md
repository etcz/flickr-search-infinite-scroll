Flickr Search (with infinite scroll)
====================================

A simple app that that uses the Flickr image search API and shows the results in a 3-column infinitely scrollable view.

This is an extension to [flickr-search](https://bitbucket.org/etcz/flickr-search/src/master/)
and essentially just adds support for pagination.
Other than that, the architecture/structure of the app is pretty much the same.

![Demo](demo.gif)

[Get the (debug) APK](https://bitbucket.org/etcz/flickr-search/src/master/infinite-scroll-debug.apk)

Implementation notes
--------------------
* Uses the AndroidX [Paging Library](https://developer.android.com/topic/libraries/architecture/paging).
* Mapped Flickr's paginated [photos search API](https://www.flickr.com/services/api/flickr.photos.search.html) to a [PageKeyedDataSource](https://developer.android.com/reference/android/arch/paging/PageKeyedDataSource).