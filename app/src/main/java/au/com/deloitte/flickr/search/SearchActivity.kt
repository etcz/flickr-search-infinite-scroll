package au.com.deloitte.flickr.search

import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import au.com.deloitte.flickr.*
import au.com.deloitte.flickr.di.DaggerAppComponent
import kotlinx.android.synthetic.main.activity_search.*
import javax.inject.Inject

class SearchActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: SearchViewModel
    lateinit var searchView: SearchView

    private var resultsAdapter: SearchResultsAdapter? = null
    private var currentQuery = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        viewModel = viewModelFrom(viewModelFactory)
        initViews()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(BUNDLE_QUERY_TEXT, currentQuery)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        currentQuery = savedInstanceState.getString(BUNDLE_QUERY_TEXT, "")
        fetchQuery(currentQuery)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.search_menu, menu)
        searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setOnQueryTextListener(this)
        if (currentQuery.isNotEmpty()) {
            searchView.onActionViewExpanded()
            searchView.clearFocus()
            searchView.setQuery(currentQuery, false)
        }
        return true
    }

    override fun onQueryTextChange(newText: String?) = true

    override fun onQueryTextSubmit(query: String?): Boolean {
        if (!query.isNullOrEmpty()) {
            currentQuery = query
            fetchQuery(query, true)
            searchView.clearFocus()
        }
        return true
    }

    private fun injectDependencies() {
        DaggerAppComponent.create()
            .searchComponent()
            .create()
            .inject(this)
    }

    private fun initViews() {
        val dividerDrawable = getDrawable(R.drawable.photo_list_divider)!!
        val horizontalDivider = DividerItemDecoration(
            this,
            DividerItemDecoration.HORIZONTAL
        ).apply {
            setDrawable(dividerDrawable)
        }
        val verticalDivider = DividerItemDecoration(
            this,
            DividerItemDecoration.VERTICAL
        ).apply {
            setDrawable(dividerDrawable)
        }
        val gridLayoutManager = GridLayoutManager(
            this,
            resources.getInteger(R.integer.search_results_columns),
            RecyclerView.VERTICAL,
            false
        )
        resultsAdapter = SearchResultsAdapter()
        photosRecyclerView.apply {
            layoutManager = gridLayoutManager
            adapter = resultsAdapter
            addItemDecoration(horizontalDivider)
            addItemDecoration(verticalDivider)
            setHasFixedSize(true)
        }
    }

    private fun fetchQuery(query: String, refreshData: Boolean = false) {
        viewModel.searchPhotos(query, refreshData)
            .observe(this, Observer { photos ->
                resultsAdapter?.submitList(photos)

                if (photos.isNullOrEmpty()) {
                    emptyStateView.show()
                    photosRecyclerView.hide()
                } else {
                    emptyStateView.remove()
                    photosRecyclerView.show()
                }
            })
    }

    companion object {
        const val BUNDLE_QUERY_TEXT = "BUNDLE_QUERY_TEXT"
    }
}
