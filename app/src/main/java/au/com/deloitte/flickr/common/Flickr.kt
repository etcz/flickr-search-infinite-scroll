package au.com.deloitte.flickr.common

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.net.MalformedURLException
import java.net.URISyntaxException
import java.net.URL

data class FlickrSearchResponse(
    @SerializedName("photos")
    val photos: FlickrPhotos
)

data class FlickrPhotos(
    @SerializedName("page")
    val page: Int,

    @SerializedName("pages")
    val pages: Int,

    @SerializedName("perpage")
    val perPage: Int,

    @SerializedName("total")
    val total: String,

    @SerializedName("photo")
    val photosList: List<FlickrPhoto>
)

@Parcelize
data class FlickrPhoto(
    @SerializedName("id")
    val id: String,

    @SerializedName("secret")
    val secret: String,

    @SerializedName("server")
    val server: String,

    @SerializedName("farm")
    val farm: Int
) : Parcelable {

    fun canonicalUrl(): String {
        return try {
            val url = URL("https://farm$farm.static.flickr.com/$server/${id}_$secret.jpg")
            url.toURI().toString()
        } catch (e: MalformedURLException) {
            ""
        } catch (e: URISyntaxException) {
            ""
        }
    }
}
