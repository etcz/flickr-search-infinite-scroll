package au.com.deloitte.flickr.search

import au.com.deloitte.flickr.common.FlickrSearchResponse
import io.reactivex.Single

interface SearchRepository {
    fun searchPhotos(text: String, page: Int = 0): Single<FlickrSearchResponse>
}