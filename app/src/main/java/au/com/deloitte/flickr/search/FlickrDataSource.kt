package au.com.deloitte.flickr.search

import androidx.paging.PageKeyedDataSource
import au.com.deloitte.flickr.common.FlickrPhoto
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class FlickrDataSource @Inject constructor(
    private val repository: SearchRepository,
    private val query: String,
    private val disposables: CompositeDisposable
) : PageKeyedDataSource<Int, FlickrPhoto>() {

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, FlickrPhoto>
    ) {
        disposables.add(
            repository.searchPhotos(text = query)
                .subscribe { response ->
                    if (response.photos.total.toInt() == 0) {
                        callback.onResult(
                            response.photos.photosList,
                            0,
                            0,
                            null,
                            null
                        )
                    } else {
                        callback.onResult(
                            response.photos.photosList,
                            response.photos.page * response.photos.perPage,
                            response.photos.total.toInt(),
                            null,
                            response.photos.page + 1
                        )
                    }
                }
        )
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, FlickrPhoto>) {
        disposables.add(
            repository.searchPhotos(text = query, page = params.key)
                .subscribe { response ->
                    var nextPage: Int? = null
                    if (response.photos.page < response.photos.pages)
                        nextPage = response.photos.page + 1
                    callback.onResult(response.photos.photosList, nextPage)
                }
        )
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, FlickrPhoto>) {
        disposables.add(
            repository.searchPhotos(text = query, page = params.key)
                .subscribe { response ->
                    var previousPage: Int? = null
                    if (response.photos.page > 0)
                        previousPage = response.photos.page - 1
                    callback.onResult(response.photos.photosList, previousPage)
                }
        )
    }
}