package au.com.deloitte.flickr.search

data class PhotoItem(
    val id: String,
    val url: String
)
