package au.com.deloitte.flickr.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import au.com.deloitte.flickr.ViewModelFactory
import au.com.deloitte.flickr.search.SearchRepository
import au.com.deloitte.flickr.search.SearchViewModel
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
object ViewModelModule {

    @JvmStatic
    @Provides
    fun viewModelFactory(
        map: Map<Class<out ViewModel>, @JvmSuppressWildcards ViewModel>
    ): ViewModelProvider.Factory = ViewModelFactory(map)

    @JvmStatic
    @Provides
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    fun searchViewModel(repository: SearchRepository): ViewModel = SearchViewModel(repository)
}