package au.com.deloitte.flickr.search

import androidx.paging.DataSource
import au.com.deloitte.flickr.common.FlickrPhoto
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class FlickrDataSourceFactory @Inject constructor(
    private val repository: SearchRepository,
    private val query: String,
    private val disposables: CompositeDisposable
) : DataSource.Factory<Int, FlickrPhoto>() {

    override fun create(): DataSource<Int, FlickrPhoto> =
        FlickrDataSource(repository, query, disposables)
}