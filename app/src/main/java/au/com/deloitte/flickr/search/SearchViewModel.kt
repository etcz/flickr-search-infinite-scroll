package au.com.deloitte.flickr.search

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import au.com.deloitte.flickr.common.BaseViewModel
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    private val repository: SearchRepository
) : BaseViewModel() {

    private var liveData: LiveData<PagedList<PhotoItem>>? = null

    fun searchPhotos(text: String, refreshData: Boolean): LiveData<PagedList<PhotoItem>> {
        if (liveData == null || refreshData) {
            val config = PagedList.Config.Builder()
                .setPageSize(PAGE_SIZE)
                .setInitialLoadSizeHint(INITIAL_LOAD_SIZE)
                .setEnablePlaceholders(false)
                .build()

            liveData = LivePagedListBuilder<Int, PhotoItem>(getDataFactory(text), config)
                .setInitialLoadKey(INITIAL_LOAD_KEY)
                .build()
        }
        return liveData as LiveData<PagedList<PhotoItem>>
    }

    private fun getDataFactory(text: String): DataSource.Factory<Int, PhotoItem> {
        return FlickrDataSourceFactory(repository, text, disposables)
            .mapByPage { photos ->
                photos.filter {
                    it.canonicalUrl().isNotEmpty()
                }.map {
                    PhotoItem(it.id, it.canonicalUrl())
                }
            }
    }

    companion object {
        private const val PAGE_SIZE = 50
        private const val INITIAL_LOAD_SIZE = 50
        private const val INITIAL_LOAD_KEY = 1
    }
}
