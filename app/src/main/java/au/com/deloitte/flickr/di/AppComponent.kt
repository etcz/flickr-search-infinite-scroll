package au.com.deloitte.flickr.di

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        NetworkModule::class,
        SubcomponentsModule::class
    ]
)
interface AppComponent {
    fun searchComponent(): SearchComponent.Factory
}
