package au.com.deloitte.flickr.search

import au.com.deloitte.flickr.common.FlickrApi
import au.com.deloitte.flickr.common.FlickrSearchResponse
import io.reactivex.Single
import javax.inject.Inject

class FlickrSearchRepository @Inject constructor(
    private val api: FlickrApi
) : SearchRepository {

    override fun searchPhotos(text: String, page: Int): Single<FlickrSearchResponse> =
        api.searchPhotos(text, page)
}