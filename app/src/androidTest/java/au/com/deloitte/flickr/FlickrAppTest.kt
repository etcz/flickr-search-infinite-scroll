package au.com.deloitte.flickr

import android.app.Application
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import au.com.deloitte.flickr.search.SearchActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class FlickrAppTest {

    @get:Rule
    val testRule = ActivityTestRule(SearchActivity::class.java)

    @Test
    fun appStart() {
        // TODO: Mock network/API request
        val packageName = ApplicationProvider.getApplicationContext<Application>().packageName
        val uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        val selector = By.pkg(packageName).depth(0)
        uiDevice.wait(Until.findObject(selector), LAUNCH_TIMEOUT_MILLIS)
    }

    companion object {
        const val LAUNCH_TIMEOUT_MILLIS = 10_000L
    }
}