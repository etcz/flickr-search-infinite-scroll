package au.com.deloitte.flickr.common

import org.junit.Assert.assertEquals
import org.junit.Test

class FlickrPhotoTest {

    @Test
    fun `given a photo, when the image URL is requested, then the correct value is returned`() {
        // given
        val photo = FlickrPhoto(
            id = "49915263792",
            secret = "f2d1c547e5",
            server = "65535",
            farm = 91
        )
        val expected = "https://farm91.static.flickr.com/65535/49915263792_f2d1c547e5.jpg"

        // when
        val actual = photo.canonicalUrl()

        // then
        assertEquals(expected, actual)
    }

    @Test
    fun `given a photo that has unsafe chars, when the image URL is requested, then an empty string is returned`() {
        // given
        val photo = FlickrPhoto(
            id = "^^",
            secret = "f2d1c547e5",
            server = "#23",
            farm = 33
        )
        val expected = ""

        // when
        val actual = photo.canonicalUrl()

        // then
        assertEquals(expected, actual)
    }
}